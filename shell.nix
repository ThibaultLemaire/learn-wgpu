{ pkgs ? import <nixpkgs> { } }:

with pkgs;

mkShell {
  buildInputs = [
    nixfmt
    rustup
    pkg-config
    # Added by winit 0.27
    cmake fontconfig
  ]
  # https://github.com/rust-windowing/winit/issues/493#issuecomment-887546424
    ++ (with xorg; [ libX11 libxcb libXcursor libXrandr libXi ]);
  # https://discourse.nixos.org/t/help-setting-up-nix-shell-with-python-and-vulkan/14512/4
  # https://github.com/gfx-rs/wgpu-rs/issues/332
  LD_LIBRARY_PATH = "${vulkan-loader}/lib";
}
